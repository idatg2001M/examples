package EventHandling;

public class Main {
    public static void main(String[] args) {
        TemperatureSensor observable = new TemperatureSensor();
        TemperatureDisplay observer1 = new TemperatureDisplay(observable);
        SmartHomeApp observer2 = new SmartHomeApp(observable);


        observable.setTemperature(10);
        observable.increaseTemperature();
    }
}
