package EventHandling;

import java.util.ArrayList;

public class NewsCollector {
    ArrayList<Subscriber> subscribers;
    String news;

    public NewsCollector() {
        this.subscribers = new ArrayList<>();
    }

    public void addSubscribers(Subscriber subscriber) {
        this.subscribers.add(subscriber);
    }

    public void setNews(String newsString) {
        this.news = newsString;
        for(Subscriber subscriber : subscribers) {
            subscriber.update(this.news);
        }
    }
}
