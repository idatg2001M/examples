package EventHandling;

import java.util.ArrayList;

public class TemperatureSensor {
    private double temperature;
    private ArrayList<TemperatureSubscriber> temperatureSubsribers;

    public TemperatureSensor() {
        this.temperature = 0;
        this.temperatureSubsribers = new ArrayList<>();
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    void increaseTemperature() {
        this.temperature++;
        updateSubsribers();
    }

    void decreaseTemperature() {
        this.temperature--;
        updateSubsribers();
    }

    void updateSubsribers() {
        for (TemperatureSubscriber subscriber : temperatureSubsribers) {
            subscriber.update();
        }
    }

    void addSubscriber(TemperatureSubscriber subscriber) {
        this.temperatureSubsribers.add(subscriber);
    }
}
