package EventHandling;

public class SmartHomeApp implements TemperatureSubscriber{
    private TemperatureSensor temperatureSensor;

    public SmartHomeApp(TemperatureSensor temperatureSensor) {
        this.temperatureSensor = temperatureSensor;
        temperatureSensor.addSubscriber(this);
    }

    public void update() {
        System.out.println("Temperature is " + temperatureSensor.getTemperature());
    }

}
