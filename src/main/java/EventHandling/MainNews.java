package EventHandling;

public class MainNews {
    public static void main(String[] args) {
        NewsCollector newsCollectorObservable = new NewsCollector();
        NRK nrk = new NRK(newsCollectorObservable);

        newsCollectorObservable.addSubscribers(nrk);
        newsCollectorObservable.setNews("Some incident happened");
        nrk.printNews();
    }
}
