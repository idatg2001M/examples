package EventHandling;

public interface Subscriber {
    void update(Object obj);
}
