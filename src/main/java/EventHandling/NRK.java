package EventHandling;

public class NRK implements Subscriber {
    String news;
    NewsCollector newsCollector;

    public NRK(NewsCollector newsCollector) {
        this.newsCollector = newsCollector;
    }

    @Override
    public void update(Object news) {
        this.news = news.toString();

    }

    public void printNews() {
        System.out.println("News: " + this.news);
    }
}
