package EventHandling;

public interface TemperatureSubscriber {
    void update();
}
