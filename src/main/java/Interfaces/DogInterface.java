package Interfaces;

public class DogInterface implements InterfaceClassAnimal {
    @Override
    public void makeSound() {
        System.out.println("Nice");
    }
    @Override
    public void eat() {
        System.out.println("Pizza time");
    }
}
