package LambdaExample;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SteamsExample {
    public static void main(String[] args) {
        /*
        List<String> stringCollection = new ArrayList<>();
        stringCollection.add("ddd2");
        stringCollection.add("aaa2");
        stringCollection.add("bbb1");
        stringCollection.add("aaa1");
        stringCollection.add("bbb3");
        stringCollection.add("ccc");
        stringCollection.add("bbb2");
        stringCollection.add("ddd1");

        List<String> stringCollectionSelected = new ArrayList<>();
        for (String s : stringCollectionSelected) {
            if(s.startsWith("a")) {
                System.out.println(s);
            }
        }
        //stringCollection => Stream => Filter => Print
        System.out.println("Using streams");
        stringCollection
                .stream()
                .filter(s -> s.startsWith("a"))
                .forEach(System.out::println);



        List<String> alphabets = Arrays.asList("a", "b", "c", "d");
        List<String> alphaUpper = alphabets
                .stream()
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.println(alphaUpper);

         */

        System.out.println("Normal Stream");

        long startTime = System.nanoTime();
        IntStream rangeSerial = IntStream.rangeClosed(1, 100);
        rangeSerial.forEach(System.out::println);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.println(duration);

        System.out.println("Parallel Stream");

        startTime = System.nanoTime();
        IntStream rangeParallel = IntStream.rangeClosed(1, 100);
        rangeParallel.parallel().forEach(System.out::println);
        endTime = System.nanoTime();
        duration = (endTime - startTime);
        System.out.println(duration);
    }
}
