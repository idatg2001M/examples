package LambdaExample;

public class LambdaExamples {
    interface Shit {
        double cool(double number);
    }

    interface Nice {
        double cool(double number, double number2);
    }

    private Shit calculateSquare = (number) -> Math.pow(number, 2);
    private Shit calculateCube = (number -> Math.pow(number, 3));
    private Shit calculateSquareRoot = (number) -> Math.sqrt(number);
    private Nice calculatePower = (number, number2) -> Math.pow(number, number2);
    private Nice calculateDifference = (number, number2) -> Math.abs(number - number2);

    public static void main(String[] args) {
        LambdaExamples main = new LambdaExamples();
        System.out.println(main.calculateSquare.cool(5));
        System.out.println(main.calculateCube.cool(5));
        System.out.println(main.calculateSquareRoot.cool(25));
        System.out.println(main.calculatePower.cool(10, 69));
        System.out.println(main.calculateDifference.cool(69, -5));
    }
}
