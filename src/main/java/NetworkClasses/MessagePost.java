package NetworkClasses;

public class MessagePost extends Post {
    private String message;

    public MessagePost(String username, String message) {
        super(username);
        this.message = message;
    }

    public void display() {
        super.display(this.getMessage());
    }

    public String getMessage() {
        return message;
    }
}
