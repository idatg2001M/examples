package NetworkClasses;

public class NetworkExample {
    public static void main(String[] args) {
        MessagePost messagePost = new MessagePost("Mikael", "message");
        messagePost.like();
        messagePost.addComment("cool message dude");
        messagePost.display();

        PhotoPost photoPost = new PhotoPost("Mikael", "caption", "file101.jpg");
        photoPost.like();
        photoPost.addComment("nice");
        photoPost.addComment("k");
        photoPost.display();
    }
}