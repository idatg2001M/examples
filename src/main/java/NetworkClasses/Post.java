package NetworkClasses;

import java.util.ArrayList;

public class Post {
    private String username;
    private long timestamp;
    private int likes;
    ArrayList<String> comments;

    public Post(String username) {
        this.username = username;
        this.timestamp = System.currentTimeMillis();
        this.likes = 0;
        this.comments = new ArrayList<>();
    }

    public void like() {
        this.likes++;
    }

    public void unlike() {
        this.likes--;
    }

    public void addComment(String comment) {
        this.comments.add(comment);
    }

    public String convertTimeToString(long timestamp) {
        long currentTimeofSystem = System.currentTimeMillis();
        long differenceMilSec = currentTimeofSystem - timestamp;
        long timeInSeconds = differenceMilSec / 1000;
        long timeInMinutes = timeInSeconds / 60;
        if (timeInMinutes > 0) {
            return timeInMinutes + " minutes";
        } else {
            return timeInSeconds + " seconds";
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public ArrayList<String> getComments() {
        return comments;
    }

    public void setComments(ArrayList<String> comments) {
        this.comments = comments;
    }

    public void displayLikes() {
        if (this.getLikes() <= 0) {
            System.out.println();
        } else if (this.getLikes() == 1) {
            System.out.println(this.getLikes() + " like");
        } else {
            System.out.println(this.getLikes() + " likes");
        }
    }

    public void displayComments() {
        if (this.getComments().size() == 0) {
            System.out.println("No comments");
        } else {
            this.getComments().forEach(System.out::println);
        }
        System.out.println();
    }

    public void display(String message) {
        System.out.println(this.getUsername());
        System.out.println(message);
        System.out.print("Post added " + this.convertTimeToString(this.getTimestamp()) + " ago - ");
        this.displayLikes();
        this.displayComments();
    }

    public void display(String fileName, String caption) {
        System.out.println(this.getUsername());
        System.out.println(fileName);
        System.out.println(caption);
        System.out.print("Post added " + this.convertTimeToString(this.getTimestamp()) + " ago - ");
        this.displayLikes();
        this.displayComments();
    }
    @Override
    public String toString() {
        String text = username + "\n" + convertTimeToString(timestamp);
        if(likes > 0) {
            text += " - " + likes + " people like this. \n";
        }
        else {
            text += "\n";
        }
        if(comments.isEmpty()) {
            return text + " No comments. \n";
        }
        else {
            return text + " " + comments.size() + " comment(s). Click here to view. \n";
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if(this == obj) {
            equals = true;
        }
        if(!(obj instanceof Post)) {
            equals = false;
        }
        Post other = (Post) obj;
        return equals;
    }
}
