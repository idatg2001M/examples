package NetworkClasses;

public class PhotoPost extends Post {
    private String caption;
    private String fileName;

    public PhotoPost(String username, String caption, String fileName) {
        super(username);
        this.caption = caption;
        this.fileName = fileName;
    }

    public void display() {
        super.display(getFileName(), getCaption());
    }

    public String getCaption() {
        return caption;
    }

    public String getFileName() {
        return fileName;
    }
}
