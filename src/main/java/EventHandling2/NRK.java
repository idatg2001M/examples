package EventHandling2;

import java.util.Observable;
import java.util.Observer;

public class NRK implements Observer {
  private String news;

  public void update(Observable observable, Object news) {
    this.news = (String) news;
  }

  public String getNews() {
    return this.news;
  }
}
