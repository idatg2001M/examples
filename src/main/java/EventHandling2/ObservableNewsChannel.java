package EventHandling2;

import java.util.Observable;

public class ObservableNewsChannel extends Observable {
  private String news;

  void setNews(String news) {
    this.news = news;
    setChanged();
    notifyObservers(news);
  }
}
