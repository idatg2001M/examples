package EventHandling2;

public class Main {
  public static void main(String[] args) {
    ObservableNewsChannel newsProvider = new ObservableNewsChannel();
    NRK observerNRK = new NRK();
    newsProvider.addObserver(observerNRK);

    newsProvider.setNews("Nice");
    System.out.println(observerNRK.getNews());
  }
}
