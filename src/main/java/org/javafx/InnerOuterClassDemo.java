package org.javafx;

public class InnerOuterClassDemo {
  public static void main(String[] args) {
    InnerOuterClassDemo obj = new InnerOuterClassDemo();
    InnerOuterClassDemo.InnerClass obj2 = obj.new InnerClass();

    System.out.println(obj2.a);
    System.out.println(obj2.b);
    System.out.println(obj2.c);
  }

  public class InnerClass {
    private int a = 10;
    public int b = 20;
    protected int c = 30;
  }
}
