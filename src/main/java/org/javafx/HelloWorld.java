package org.javafx;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HelloWorld extends Application implements EventHandler<ActionEvent> {

  private Button btn1;
  private Button btn2;
  private Logger logger = LoggerFactory.getLogger(HelloWorld.class);

  private MenuBar createMenus() {
    MenuBar mainMenu = new MenuBar();
    Menu menuFile = new Menu("File");
    MenuItem openFile = new Menu("Open");

    mainMenu.getMenus().addAll(menuFile);
    menuFile.getItems().add(openFile);


    Menu editFile = new Menu("Edit");
    MenuItem copyFile = new Menu("Copy");

    mainMenu.getMenus().addAll(editFile);
    editFile.getItems().add(copyFile);

    return mainMenu;
  }

  @Override
  public void start(Stage primaryStage) {

    BorderPane root = new BorderPane();
    VBox topContainer = new VBox();
    GridPane gridPane = new GridPane();

    //Place the menubar in the topContainer
    topContainer.getChildren().add(createMenus());
    btn1 = new Button("Get User info");
    btn1.setOnAction(env -> getUserInformation());

    AtomicBoolean userConfirmedOK = new AtomicBoolean(false);
    btn2 = new Button("Button 2");
    btn2.setOnAction(e -> showInformation());


    gridPane.getChildren().add(btn1);
    gridPane.getChildren().add(btn2);
    GridPane.setRowIndex(btn1, 0);
    GridPane.setColumnIndex(btn1, 1);
    GridPane.setRowIndex(btn2, 1);
    GridPane.setColumnIndex(btn2, 1);

    //Place the topContainer in the top-section of theBorderPane
    root.setTop(topContainer);
    root.setLeft(gridPane);

    Scene scene = new Scene(root, 300, 250);

    primaryStage.setTitle("Menu Sample");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  @Override
  public void stop() {
    System.exit(0);
  }

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void handle(ActionEvent event) {

  }

  public void getUserInformation() {
    TextInputDialog dialog = new TextInputDialog();
    dialog.setTitle("User Information");

    Label labelName = new Label("Name: ");
    Label labelPhone = new Label("Phone: ");
    TextField textName = new TextField();
    TextField textPhone = new TextField();

    GridPane grid = new GridPane();
    grid.add(labelName, 1, 1);
    grid.add(textName, 2, 1);
    grid.add(labelPhone, 1, 2);
    grid.add(textPhone, 2, 2);
    dialog.getDialogPane().setContent(grid);
    dialog.showAndWait();
  }

  /**
   * This method makes a pop up dialog appear.
   */
  public void showInformation() {

    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    alert.setTitle("Alert Information");
    alert.setHeaderText("Alert Header");
    alert.setContentText("This is a very long info text!");

    Optional<ButtonType> result = alert.showAndWait();
    if (result.get() == ButtonType.OK) {
      logger.info("Share the video");
    } else {
      logger.info("I did not share the video");
    }
  }

}
