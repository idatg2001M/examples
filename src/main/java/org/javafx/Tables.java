package org.javafx;


import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Tables extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {

    TableColumn<Person, String> nameColumn = new TableColumn<>("Name");
    TableColumn<Person, Integer> ageColumn = new TableColumn<>("age");
    nameColumn.setMinWidth(200);
    ageColumn.setMinWidth(200);

    nameColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("name"));
    ageColumn.setCellValueFactory(new PropertyValueFactory<Person, Integer>("age"));

    TableView<Person> table = new TableView<>();
    table.setItems(getPerson());

    table.getColumns().addAll(nameColumn, ageColumn);

    VBox pane = new VBox();
    pane.getChildren().add(table);
    Scene scene = new Scene(pane, 300, 100);

    primaryStage.setTitle("Person Age Demo");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }

  /**
   * Returns an ObservableList of three Persons.
   * @return ObservableList
   */
  public ObservableList<Person> getPerson() {
    ObservableList<Person> persons = FXCollections.observableArrayList();

    Person person1 = new Person("Magnus", 22);
    Person person2 = new Person("Mikael", 20);
    Person person3 = new Person("Mikkel", 5);
    persons.add(person1);
    persons.add(person2);
    persons.add(person3);
    return persons;
  }
}
