package org.designpattern;

public class ShapeFactory {
  private static Shape shape = null;
  public static Shape getShapes(String shapeType) {
    if(shapeType == null) {
      return null;
    }
    if (shapeType.equalsIgnoreCase("CIRCLE")) {
      if (shape == null) {
        shape = new Circle();
      }
      return shape;
    }
    else if(shapeType.equalsIgnoreCase("RECTANGLE")) {
      if (shape == null) {
        shape = new Rectangle();
      }
      return shape;
    }
    else if(shapeType.equalsIgnoreCase("TRIANGLE")) {
      if (shape == null) {
        shape = new Triangle();
      }
      return shape;
    }
    return null;
  }
}
