package org.designpattern;

public class DrawShapes {

  public static void main(String[] args) {
    Shape shape1 = new Circle();
    Shape shape2 = new Triangle();
    Shape shape3 = new Rectangle();

    shape1.draw();
    shape2.draw();
    shape3.draw();

    Shape shape4 = ShapeFactory.getShapes("Circle");
    Shape shape5 = ShapeFactory.getShapes("Rectangle");
    Shape shape6 = ShapeFactory.getShapes("Triangle");
    shape4.draw();
    shape5.draw();
    shape6.draw();
  }
}
