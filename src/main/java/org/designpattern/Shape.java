package org.designpattern;

public interface Shape {
  void draw();
}
