package org.studentdatabase;

import java.util.ArrayList;

public class StudentListPlain implements StudentList {
  private ArrayList<Student> studentList;

  public StudentListPlain() {
    this.studentList = new ArrayList<>();
  }

  @Override
  public void addStudents(Student student) {
    this.studentList.add(student);
  }

  @Override
  public void printStudents() {
    studentList.forEach(Student::printStudent);
  }

  @Override
  public ArrayList<Student> getStudentList() {
    return studentList;
  }
}
