package org.studentdatabase;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class StudentDAO implements StudentList {
  private final EntityManagerFactory emf;
  private EntityManager em;

  public StudentDAO() {
    this.emf = Persistence.createEntityManagerFactory("name");
    this.em = emf.createEntityManager();
  }

  @Override
  public void addStudents(Student student) {
    this.em.getTransaction().begin();
    this.em.merge(student);
    this.em.flush();
    this.em.getTransaction().commit();
  }

  @Override
  public void printStudents() {
    String jpql = "SELECT s FROM Student s WHERE s.studentName = 'Mikael'";
    Query query = this.em.createQuery(jpql);
    List<Student> studentList = query.getResultList();
    studentList.forEach(Student::printStudent);
  }

  @Override
  public ArrayList<Student> getStudentList() {
    return null;
  }
}
