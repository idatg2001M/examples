package org.studentdatabase;


public class StudentDBApp {
  private StudentList studentList;


  public StudentDBApp() {

    studentList = new StudentDAO();
  }

  void addToDatabase() {
    this.studentList.addStudents(new Student(100, "Amund2", "BIDAT", "Year 1"));
    this.studentList.addStudents(new Student(111, "Mikael", "BIDAT", "Year 1"));
    this.studentList.addStudents(new Student(123, "Magnus", "BIDAT", "Year 1"));
  }

  void printFromDatabase() {
    studentList.printStudents();
  }

  public static void main(String[] args) {
    StudentDBApp studentDBApp = new StudentDBApp();
    studentDBApp.addToDatabase();
    studentDBApp.printFromDatabase();
  }
}
