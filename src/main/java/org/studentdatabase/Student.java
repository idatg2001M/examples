package org.studentdatabase;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Student implements Serializable {
  @Id
  String studentName;
  Integer studentNumber;
  String studentProgram;
  String studyYear;

  public Student(Integer studentNumber, String studentName, String studentProgram, String studyYear) {
    this.studentNumber = studentNumber;
    this.studentName = studentName;
    this.studentProgram = studentProgram;
    this.studyYear = studyYear;
  }

  public Student() {
  }

  public String getStudentName() {
    return studentName;
  }

  public void setStudentName(String studentName) {
    this.studentName = studentName;
  }

  public Integer getStudentNumber() {
    return studentNumber;
  }

  public void setStudentNumber(Integer studentNumber) {
    this.studentNumber = studentNumber;
  }

  public String getStudentProgram() {
    return studentProgram;
  }

  public void setStudentProgram(String studentProgram) {
    this.studentProgram = studentProgram;
  }

  public String getStudyYear() {
    return studyYear;
  }

  public void setStudyYear(String studyYear) {
    this.studyYear = studyYear;
  }

  void printStudent() {
    System.out.println(getStudentName());
    System.out.println(getStudentNumber());
    System.out.println(getStudyYear());
    System.out.println(getStudentProgram());
  }
}
