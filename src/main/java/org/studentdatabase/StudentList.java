package org.studentdatabase;

import java.util.ArrayList;

public interface StudentList {
  void addStudents(Student student);

  void printStudents();

  ArrayList<Student> getStudentList();
}
