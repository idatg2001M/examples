package org.iostreams;

import java.io.*;

public class IOStreams {
  public static void main(String[] args) throws Exception {
    FileReader nice = new FileReader("/home/ukhu/Documents/data.txt");
    int data;
    while ((data = nice.read()) != -1) {
      System.out.print((char) data);
    }
    nice.close();

    InputStream input2 = new BufferedInputStream(new FileInputStream("/home/ukhu/Documents/data.txt"));
    OutputStream output = new FileOutputStream("/home/ukhu/Documents/data2.txt");
    int data2 = input2.read();
    while (data2 != -1) {
      System.out.print((char)data2);
      output.write(data2);
      data2 = input2.read();
    }
  }
}
