package org.icecream;

import static lib.Out.*;

public class IcecreanDecoratorClient {

	public static void main(String args[]) {
		
		Icecream simpleIcecream = new SimpleIcecream();
		out(simpleIcecream.makeIcecream());
		
		Icecream nuttyIcecream = new NuttyDecorator(new SimpleIcecream());
		out(nuttyIcecream.makeIcecream());

		Icecream honeyIcecream = new HoneyDecorator(new SimpleIcecream());
		out(honeyIcecream.makeIcecream());
		
		Icecream honeyNuttyIcecream = new HoneyDecorator(new NuttyDecorator(new SimpleIcecream()));
		out(honeyNuttyIcecream.makeIcecream());
	}
}
