/*
 * Book.java
 *
 * November 2020, av Tomas Holt
 */

package org.databases;

import javax.persistence.*;
import java.io.*;

@Entity
@NamedQuery(name="findNumberOfBooks", query="SELECT COUNT(o) from Book o")
public class Book implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String isbn;
    private String title;
    private double price;
    private String author;
    private int numberOfPages;
   
    public Book(){}//m ha en konstruktor med tom parameterliste ihht JavaBeans standarden
    
    public Book(String isbn, String title, double price, String author, int numberOfPages){
        this.isbn = isbn;
        this.title = title;
        this.price = price;
        this.author = author;
        this.numberOfPages = numberOfPages;
    }
    
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
    
    public String getTitle(){
        return title;
    }
    
    public void setTitle(String title){
        this.title = title;
    }
    
    public double getPrice(){
        return price;
    }
    
    public void setPrice(double price){
        this.price = price;
    }
    
    public String getAuthor(){
        return author;
    }
    
    public void setAuthor(String author){
        this.author = author;
    }
    
    public int getNumberOfPages(){
        return numberOfPages;
    }
    
    public void setNumberOfPages(int ant){
        this.numberOfPages = ant;
    }
    
    public String toString(){
        return "Book: Isbn: " + isbn + ". Title: " + title + ". Author " + author + ". Price: " + price + ". Number of pages: " + numberOfPages;
    }
}
