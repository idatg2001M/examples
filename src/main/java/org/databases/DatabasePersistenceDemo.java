package org.databases;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DatabasePersistenceDemo {

  public static void main(String[] args) {
    StudentData studentData = new StudentData();
    studentData.setStudentName("Kristian");
    studentData.setAge(21);
    studentData.setStudentNumber(111222);
    studentData.setMessage("Kult");


    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("StudentDatabase");
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    StudentData readFromDatabase;
    readFromDatabase = entityManager.find(StudentData.class, 32);
    System.out.println(readFromDatabase);

    entityManager.getTransaction().begin();
    entityManager.persist(studentData);
    entityManager.getTransaction().commit();

  }
}
