/*
 * BookDAO.java
 *
 * Created on Mars 18, 2020, by Tomas Holt
 * Comment: Exception handling done as simple as possible.
 * Exceptions thrown by EntityManger inherits Persistence Exception.
 * All such exceptions will rollback the transaction => nothing is stored in the database.
 * If a rollback occur, all the entities will be detached, caused by the fact that in this
 * case it is impossible to know the integrity of the objects/entities.
 *
 * One solution to this problem is to close the EntityManager and try with a new EntityManager.
 * A EntityManger is a light weight object, and the cost of create such objects is not costly.
 * The opposite is true for the EntityManagerFactory - which will make connection(s) to the database.
 */

package org.databases;

import javax.persistence.*;
import java.util.List;

public class BookDAO {
    private EntityManagerFactory emf;
    /* NB! EntityManagerFactory is thread safe, EntityManger is not!
    * The make the solution thread safe use/create the EntityManger locally when needed, do not keep
    * as object variable.
    */

    public BookDAO(EntityManagerFactory emf){
        this.emf = emf;
    }
    
    /*Create new books.
    * Persist works as SQL INSERT.
    * The book (ISBN) can not be stored before, the ISBN must be unique.
     */
    public void storeNewBook(Book book){
        EntityManager em  = getEM();
        try{
             em.getTransaction().begin();
             em.persist(book);// book into persistence context
             em.getTransaction().commit();//store in database
        }finally{
            closeEM(em);
        }
    }
    
    //Find a book based on the primary key, ie. ISDN
    public Book findBook(String isbn){
        EntityManager em = getEM();
        try{
            return em.find(Book.class, isbn);
        }finally{
            closeEM(em);
        }
    }
    
    // Edit an existing book, use merge to ensure that the book gets into the persistence context.
    public void editBook(Book book){
        EntityManager em = getEM();
        try{
            em.getTransaction().begin();
            Book b = em.merge(book);//the book/entity into persistence context
            em.getTransaction().commit();//store in database
        }finally{
            closeEM(em);
        }
    }

    //delete a chosen book
    public void deleteBook(String isbn){
        EntityManager em = getEM();
        try{
            Book b = findBook(isbn);
            em.getTransaction().begin();
            em.remove(b);//remove must be called in a transaction
            em.getTransaction().commit();
        }finally{
            closeEM(em);
        }
    }
    
    //get all the books
    public List<Book> getAllBooks(){
        EntityManager em = getEM();
        try{
            Query q = em.createQuery("SELECT OBJECT(o) FROM Book o");
            //SELECT o FROM Book o will give the sames result
            //NB! Book needs capital B (same as the class name)
            return q.getResultList();
        }finally{
            closeEM(em);
        }
    }
    
    //get number of books, using a @NamedQuery. You will find this in the book class.
    public int getNumberOfBooks(){
        EntityManager em = getEM();
        try{
            Query q = em.createNamedQuery("findNumberOfBooks");
            Long ant = (Long)q.getSingleResult();
            return ant.intValue();
        }finally{
            closeEM(em);
        }
    }
    
    //Merk at begge sprringene i metoden fungerer (en utkommentert) 
    //Ofte kan nok den frste vre  foretrekke.
    public List<Book> getBooksByAuthor(String author){
        EntityManager em = getEM();
        try{
            Query q = em.createQuery("SELECT OBJECT(a) FROM Book a WHERE a.author= :author");
            q.setParameter("author",author);
            return q.getResultList();
        }finally{
            closeEM(em);
        }
    }
     
    private EntityManager getEM(){
        return emf.createEntityManager();
    }
    
    private void closeEM(EntityManager em){
        if (em != null && em.isOpen()) em.close();
    }
       
    /* Simple test client" */
    public static void main(String args[]) throws Exception{
        EntityManagerFactory emf = null;
        BookDAO facade = null;
        System.out.println("start...");
        try{
            emf = Persistence.createEntityManagerFactory("LecturePU");
	        //LecturePU=Persistence Unit Name, see persistence.xml

            System.out.println("EntityManagerFactory created " + emf);
            facade = new BookDAO(emf);
            System.out.println("BookDAO created");
            
            //make a book using the set methods
            Book book = new Book();
            book.setIsbn("ISBN-1");
            book.setNumberOfPages(33);
            book.setAuthor("Lille");
            book.setPrice(300);
            book.setTitle("Testbook1");
            facade.storeNewBook(book);
            
            //make new book with constructor instead of the set methods
            book = new Book("ISBN-2","Testbook2",200,"Author2",50);
            facade.storeNewBook(book);
            
            //List books
            System.out.println("These books are stored in the database:");
            List<Book> bookList = facade.getAllBooks();
            for (Book b : bookList){
                System.out.println("---" + b);
            }
            
            book = (Book)bookList.get(0);
            book.setTitle("Title changed");
            facade.editBook(book);
            
            book = facade.findBook(book.getIsbn());//fetch book
            System.out.println("Book changed and is now like this: " + book);
            
            //find number of books in database
            int numberOfBooks = facade.getNumberOfBooks();
            System.out.println("There are " + numberOfBooks + " books in the database");
            
            //list books by a chosen author
            String theAuthor = "Author2";
            bookList = facade.getBooksByAuthor(theAuthor);
            System.out.println(" These books exists made by " + theAuthor + " " + bookList.size());
            for (Book b : bookList){
                System.out.println("\t" + b.getTitle());
            }
        } finally{
            emf.close();
        }
    }
}