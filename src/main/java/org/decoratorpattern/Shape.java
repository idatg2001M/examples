package org.decoratorpattern;

public interface Shape {
  void draw();
}
