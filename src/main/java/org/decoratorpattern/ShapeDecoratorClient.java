package org.decoratorpattern;

public class ShapeDecoratorClient {
  public static void main(String[] args) {
    Shape shape = new Circle();
    shape.draw();

    Shape shape2 = new Rectangle();
    shape2.draw();

    Shape circle = new Circle();
    Shape redCircle = new RedShapeDecorator(new Circle());
    redCircle.draw();

    Shape redRectangle = new RedShapeDecorator(new Rectangle());
    redRectangle.draw();
  }
}
