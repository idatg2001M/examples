package org.decoratorpattern;

public class RedShapeDecorator implements Shape{
  Shape shape = null;

  public RedShapeDecorator(Shape shape) {
    this.shape = shape;
  }
  @Override
  public void draw() {
    shape.draw();
    setBorder(shape);
  }

  public void setBorder(Shape decoratorShape) {
    System.out.println("Border color :: Red");
  }
}
