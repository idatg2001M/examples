package org.temperaturesensors;

import java.util.ArrayList;

public class TemperatureSensor {
    private double temperature;
    private ArrayList<TemperatureSubscriber> temperatureSubscribers;

    public TemperatureSensor() {
        this.temperature = 0;
        this.temperatureSubscribers = new ArrayList<TemperatureSubscriber>();
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    void increaseTemperature()
    {
        this.temperature++;
        updateSubscribers();
    }

    void decreaseTemperature()
    {
        this.temperature--;
        updateSubscribers();
    }

    void updateSubscribers()
    {
        for (TemperatureSubscriber subscribers:temperatureSubscribers)
        {
            subscribers.update();
        }
    }

    void addSubscriber(TemperatureSubscriber tempSubscriber)
    {
        this.temperatureSubscribers.add(tempSubscriber);
    }
}
