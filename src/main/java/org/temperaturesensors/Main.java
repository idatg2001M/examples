package org.temperaturesensors;

public class Main
{
    public static void main(String[] args) {
        TemperatureSensor observable = new TemperatureSensor();
        TemperatureDisplay observer1 = new TemperatureDisplay(observable);
        SmartHomeApp observer2 = new SmartHomeApp(observable);

        observable.addSubscriber(observer1);
        observable.addSubscriber(observer2);

        observable.setTemperature(10);
        observable.updateSubscribers();
        //observer1.update();
        //observer2.update();

        observable.increaseTemperature();
        observer1.update();
        observer2.update();
    }
}
