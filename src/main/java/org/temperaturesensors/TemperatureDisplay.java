package org.temperaturesensors;

public class TemperatureDisplay implements TemperatureSubscriber
{

    private TemperatureSensor temperatureSensor;


    public TemperatureDisplay(TemperatureSensor temperatureSensor) {
        this.temperatureSensor = temperatureSensor;
        temperatureSensor.addSubscriber(this);
    }

    public void update()
    {
        System.out.println("Display:: Temperature is " + temperatureSensor.getTemperature());
    }
}
