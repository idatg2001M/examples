package org.temperaturesensors;

public interface TemperatureSubscriber {
    void update();
}
