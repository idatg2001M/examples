package org.filehandling;

import org.javafx.HelloWorld;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileStreamExample {


  public static void main(String[] args) throws IOException {
    Logger logger = LoggerFactory.getLogger(HelloWorld.class);
    /*int a = 100;
    try (FileWriter fileOutputStream = new FileWriter("FileStreamExample")) {
      fileOutputStream.write(a);
    }

    try(FileInputStream fileInputStream = new FileInputStream("FileStreamExample")) {
      int readValue;
      while((readValue = fileInputStream.read()) != -1) {
        logger.info("Read value is -> " + readValue);
      }
    }*/
    /*StudentDataClass studentDataClass = new StudentDataClass(10, "ok", "Mikael", 25000);
    ObjectOutputStream objectOutputStream = null;
    try(FileOutputStream fileOutputStream = new FileOutputStream("SerializationExample.idat")) {
      objectOutputStream = new ObjectOutputStream(fileOutputStream);
      objectOutputStream.writeObject(studentDataClass);
    }



    ObjectInputStream objectInputStream = null;
    StudentDataClass studentDataClass2 = null;
    try(FileInputStream fileInputStream = new FileInputStream("SerializationExample.idat")) {
      objectInputStream = new ObjectInputStream(fileInputStream);
       studentDataClass2 = (StudentDataClass) objectInputStream.readObject();
      System.out.println(studentDataClass2.studentName);
    } catch (ClassNotFoundException e) {
      logger.error("Class not found");
    }*/


    StudentDataClass studentDataClass1 = new StudentDataClass(21, "Hei1", "Mikael1", 25001);
    StudentDataClass studentDataClass2 = new StudentDataClass(22, "Hei2", "Mikael2", 25002);
    StudentDataClass studentDataClass3 = new StudentDataClass(23, "Hei3", "Mikael3", 25003);
    StudentDataClass studentDataClass4 = new StudentDataClass(24, "Hei4", "Mikael4", 25004);
    List<StudentDataClass> listStudents = new ArrayList<>();
    listStudents.add(studentDataClass1);
    listStudents.add(studentDataClass2);
    listStudents.add(studentDataClass3);
    listStudents.add(studentDataClass4);


    ObjectOutputStream objectOutputStream = null;
    try (FileOutputStream fileOutputStream = new FileOutputStream("SerializationMultipleExampleList.idat")) {
      objectOutputStream = new ObjectOutputStream(fileOutputStream);
      for (StudentDataClass student : listStudents) {
        objectOutputStream.writeObject(student);
      }
      objectOutputStream.writeObject(null);
    }


    ObjectInputStream objectInputStream = null;
    StudentDataClass student = null;
    try (FileInputStream fileInputStream = new FileInputStream("SerializationMultipleExampleList.idat")) {
      objectInputStream = new ObjectInputStream(fileInputStream);
      Object object;
      while ((object = objectInputStream.readObject()) != null) {
        student = (StudentDataClass) object;
        System.out.println(student.studentName);
        System.out.println(student.studentNumber);
        System.out.println(student.a);
        System.out.println(student.message);
      }
    } catch (ClassNotFoundException e) {
      logger.info("Class not found");
    }
  }
}
