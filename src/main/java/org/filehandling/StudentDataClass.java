package org.filehandling;

import java.io.Serializable;

public class StudentDataClass implements Serializable {
  public int a;
  public String message;
  public String studentName;
  public int studentNumber;

  public StudentDataClass(int a, String message, String studentName, int studentNumber) {
    this.a = a;
    this.message = message;
    this.studentName = studentName;
    this.studentNumber = studentNumber;
  }
}
