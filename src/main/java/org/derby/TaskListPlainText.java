package org.derby;

import java.util.ArrayList;
import java.util.Iterator;

public class TaskListPlainText implements TaskList{

  private ArrayList<TodoTask> tasks;

  public TaskListPlainText() {
    this.tasks = new ArrayList<>();
  }

  @Override
  public void addTodoTask(TodoTask todoTask) {
    tasks.add(todoTask);
  }

  @Override
  public void close() {

  }

  @Override
  public Iterator<TodoTask> iterator() {
    return tasks.iterator();
  }
}
