package org.derby;

import java.util.Iterator;

public interface TaskList extends Iterable<TodoTask>{
  void addTodoTask(TodoTask todoTask);
  void close();

  @Override
  Iterator<TodoTask> iterator();
}
