package org.derby;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class TodoApp {
  private static TaskList taskList;
  private static EntityManagerFactory entityManagerFactory;
  private static EntityManager entityManager;

  public TodoApp() {
    this.taskList = new TaskListPlainText();
    entityManagerFactory = Persistence.createEntityManagerFactory("todo-pu");
    entityManager = entityManagerFactory.createEntityManager();
  }

  void fillWithTask() {
    taskList.addTodoTask(new TodoTask("Task 1", "Get the milk"));
    taskList.addTodoTask(new TodoTask("Task 2", "Get the newspaper"));
    taskList.addTodoTask(new TodoTask("Task 3", "Brew coffee"));
    taskList.addTodoTask(new TodoTask("Task 4", "Get the bread"));
    taskList.addTodoTask(new TodoTask("Task 5", "Walk the dog"));
  }

  void printAllTasks() {
    for(TodoTask todoTask : taskList) {
      System.out.println("Tasks: " +
              todoTask.getDescription() +
              todoTask.getSummary());
    }
  }

  void close() {
    this.entityManagerFactory.close();
    taskList.close();
  }

  public static void main(String[] args) {
    TodoApp todoApp = new TodoApp();
    /*
    Query databaseQuery = entityManager.createQuery(jdbcQuery);
    taskList = (TaskList) databaseQuery.getResultList().iterator();

     */

    ArrayList<TodoTask> todoTaskNew = new ArrayList<>();
    todoTaskNew.add(new TodoTask("Task 1", "Get the milk"));
    todoTaskNew.add(new TodoTask("Task 2", "Get the newspaper"));
    todoTaskNew.add(new TodoTask("Task 3", "Brew coffee"));
    todoTaskNew.add(new TodoTask("Task 4", "Get the bread"));
    todoTaskNew.add(new TodoTask("Task 5", "Walk the dog"));

/*
    entityManager.getTransaction().begin();
    entityManager.persist(todoTaskNew.get(0));
    entityManager.getTransaction().commit();

 */
    todoApp.fillWithTask();
    //todoApp.printAllTasks();


    String jdbcQuery = "SELECT t FROM TodoTask t";
    Query databaseQuery = entityManager.createQuery(jdbcQuery);
    List nice = databaseQuery.getResultList();
    //System.out.println(nice.get(0).getDescription());
    System.out.println(nice);

    todoApp.close();
  }

}
