package InheritanceExample;

public class Dog extends Animal{
    public Dog(String color, String race) {
        super(color, race);
    }

    public void bark() {
        System.out.println("Woof woof");
    }
}
