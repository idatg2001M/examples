package InheritanceExample;

public class Cat extends Animal{
    public Cat(String color, String race) {
        super(color, race);
    }

    public void meow() {
        System.out.println("Meow meow");
    }
}
