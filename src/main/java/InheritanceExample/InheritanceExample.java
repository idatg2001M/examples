package InheritanceExample;

public class InheritanceExample {

    public static void main(String[] args) {
        System.out.println("InheritanceExample.Cat object Data");
        Cat cat1 = new Cat("Black", "Tiger");
        cat1.eat();
        cat1.move();
        cat1.sleep();
        cat1.meow();
        System.out.println(cat1.getColor());
        System.out.println(cat1.getRace());

        System.out.println("---------------------------------------");

        System.out.println("InheritanceExample.Dog object Data");
        Dog dog1 = new Dog("Grey", "Chihuahua");
        dog1.eat();
        dog1.move();
        dog1.sleep();
        dog1.bark();
        System.out.println(dog1.getColor());
        System.out.println(dog1.getRace());
    }
}
