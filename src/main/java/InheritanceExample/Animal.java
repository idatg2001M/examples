package InheritanceExample;

public class Animal {
    private String color;
    private String race;

    public Animal(String color, String race) {
        this.color = color;
        this.race = race;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public void eat() {
        System.out.println("I can eat.");
    }

    public void move() {
        System.out.println("I can move fast.");
    }

    public void sleep() {
        System.out.println("I can sleep.");
    }
}
